package main

import (
	"bytes"
	"git.sitilge.id.lv/advent-of-code/src/day5"
	"git.sitilge.id.lv/advent-of-code/src/helpers"
	"log"
	"os"
)

func main() {
	file, err := os.Open("./inputs/day5.txt")
	if err != nil {
		log.Fatal("unable to open inputs file")
	}
	defer file.Close()

	lines := helpers.ReadLines(file)

	split := bytes.Split(lines[0], []byte{','})

	inputs := helpers.ParseInt(split)

	day5.Task1(inputs, 1)
}