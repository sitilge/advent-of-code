# advent-of-code

Solutions for Advent of Code 2019.

## Notes

- Day 2. Everything in Go is passed by value. Slices too. But a slice value is a header, describing a contiguous section of a backing array, and a slice value only contains a pointer to the array where the elements are actually stored. The slice value does not include its elements (unlike arrays).
  
  So when you pass a slice to a function, a copy will be made from this header, including the pointer, which will point to the same backing array. Modifying the elements of the slice implies modifying the elements of the backing array, and so all slices which share the same backing array will "observe" the change.
  
  Source: https://stackoverflow.com/questions/39993688/are-golang-slices-passed-by-value
  
 - Day 3. Do math (unlike me). I did it brute force, O(n^2) complexity and huge n ~ 30k.
 
 - Day 4. Do not use string to convert from int. Use strconv.Itoa().