package helpers

import (
	"fmt"
	"io"
	"strconv"
)

func ReadLines(reader io.Reader) [][]byte {
	buff := make([]byte, 64)

	var lines [][]byte
	var parsed []byte

	for {
		n, err := reader.Read(buff)

		if err == io.EOF {
			break
		}

		b := buff[:n]

		//parsing integers
		for i := range b {
			if b[i] == '\n' {
				lines = append(lines, parsed)

				parsed = nil

				continue
			}

			parsed = append(parsed, b[i])
		}
	}

	lines = append(lines, parsed)

	return lines
}

func ParseInt(bytes [][]byte) []int {
	var inputs []int

	for _, line := range bytes {
		i, err := strconv.Atoi(string(line))

		if err != nil {
			fmt.Printf("unable to conver to int %v \n", string(line))

			continue
		}

		inputs = append(inputs, i)
	}

	return inputs
}
