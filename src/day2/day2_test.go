package day2

import (
	"bytes"
	"git.sitilge.id.lv/advent-of-code/src/helpers"
	"log"
	"os"
	"testing"
)

var inputs []int

func TestMain(m *testing.M) {
	file, err := os.Open("./../../inputs/day2.txt")
	if err != nil {
		log.Fatal("unable to open inputs file")
	}
	defer file.Close()

	lines := helpers.ReadLines(file)

	split := bytes.Split(lines[0], []byte{','})

	inputs = helpers.ParseInt(split)

	code := m.Run()

	os.Exit(code)
}

func TestTask1(t *testing.T) {
	if Task1(inputs, 12, 2) != 7594646 {
		t.Fail()
	}
}

func TestTask2(t *testing.T) {
	result, err := Task2(inputs, 19690720)

	if err != nil {
		t.Fail()
	}

	if result != 3376 {
		t.Fail()
	}
}
