package day2

import (
	"fmt"
	"log"
)

func Task1(opcodes []int, noun int, verb int) int {
	if len(opcodes) < 4 {
		log.Fatal("Invalid opcode length")
	}

	results := make([]int, len(opcodes))
	copy(results, opcodes)

	//Funny requirements
	results[1] = noun
	results[2] = verb

	for i := 0; i < len(results); i += 4 {
		opcode := results[i]
		switch opcode {
		case 1:
			results[results[i+3]] = results[results[i+1]] + results[results[i+2]]
		case 2:
			results[results[i+3]] = results[results[i+1]] * results[results[i+2]]
		case 99:
			return results[0]
		default:
			log.Fatalf("Invalid opcode %v at position %v, with noun %v and verb %v \n", opcode, i, noun, verb)
		}
	}

	return results[0]
}

func Task2(opcodes []int, result int) (int, error) {
	for i := 0; i < 100; i++ {
		for j := 0; j < 100; j++ {
			if Task1(opcodes, i, j) == result {
				return 100*i + j, nil
			}
		}
	}

	return 0, fmt.Errorf("No such noun and verb found to produce %v \n", result)
}
