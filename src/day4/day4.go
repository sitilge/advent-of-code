package day4

import (
	"log"
	"strconv"
)

func SplitInt(number int) []int {
	ascii := strconv.Itoa(number)

	var output []int

	for _, v := range ascii {
		converted, _ := strconv.Atoi(string(v))
		output = append(output, converted)
	}

	return output
}

func Calc1(min int, max int) [][]int {
	if min < 100000 || max > 999999 {
		log.Fatal("Invalid range")
	}

	var combinations1 [][]int

	for input := min; input <= max; input++ {
		number := SplitInt(input)

		var found bool

		for i := range number {
			digitCurr := number[i]

			if i > 0 {
				digitPrev := number[i- 1]

				if digitCurr < digitPrev {
					found = false

					break
				} else if digitCurr == digitPrev {
					found = true
				}
			}
		}

		if found {
			combinations1 = append(combinations1, number)
		}
	}

	return combinations1
}

func Calc2(combinations1 [][]int) [][]int {
	var combinations2 [][]int

	for _, number := range combinations1 {
		var found bool

		for i := 0; i < len(number); i++ {
			digitCurr := number[i]

			if i > 0 {
				digitPrev := number[i-1]

				if digitCurr == digitPrev {
					if i + 1 < len(number) {
						digitNext := number[i+1]

						if digitCurr == digitNext {
							//found = false

							for j := i; j < len(number); j++ {
								if j + 1 < len(number) {
									if digitCurr == number[j + 1] {
										i = i + 1
									} else {
										break
									}
								}
							}

							continue
						} else {
							found = true
						}
					} else {
						found = true
					}
				}
			}
		}

		if found {
			combinations2 = append(combinations2, number)
		}
	}
	
	return combinations2
}

func Task1(min int, max int) int {
	result1 := Calc1(min, max)
	
	return len(result1)
}

func Task2(min int, max int) int {
	result1 := Calc1(min, max)
	result2 := Calc2(result1)

	return len(result2)
}