package day4

import (
	"bytes"
	"git.sitilge.id.lv/advent-of-code/src/helpers"
	"log"
	"os"
	"strconv"
	"testing"
)

var min, max int

func TestMain(m *testing.M) {
	file, err := os.Open("./../../inputs/day4.txt")
	if err != nil {
		log.Fatal("unable to open inputs file")
	}
	defer file.Close()

	lines := helpers.ReadLines(file)

	inputs := bytes.Split(lines[0], []byte{'-'})

	//TODO - check errors
	min, _ = strconv.Atoi(string(inputs[0]))
	max, _ = strconv.Atoi(string(inputs[1]))

	code := m.Run()

	os.Exit(code)
}

func TestTask1(t *testing.T) {
	if Task1(min, max) != 1890 {
		t.Fail()
	}
}

func TestTask2(t *testing.T) {
	if Task2(min, max) != 1277 {
		t.Fail()
	}
}