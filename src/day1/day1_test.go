package day1

import (
	"git.sitilge.id.lv/advent-of-code/src/helpers"
	"log"
	"os"
	"testing"
)

var inputs []int

func TestMain(m *testing.M) {
	file, err := os.Open("./../../inputs/day1.txt")
	if err != nil {
		log.Fatal("unable to open inputs file")
	}
	defer file.Close()

	lines := helpers.ReadLines(file)
	inputs = helpers.ParseInt(lines)

	code := m.Run()

	os.Exit(code)
}

func TestTask1(t *testing.T) {
	result11, _ := Task1(&inputs)

	if result11 != 3371958 {
		t.Log("Task1 failed")
		t.Fail()
	}
}

func TestTask2(t *testing.T) {
	_, result12 := Task1(&inputs)

	if result12 != 1683092 {
		t.Log("Task2 failed")
		t.Fail()
	}
}
