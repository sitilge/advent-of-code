package day1

func Task1(masses *[]int) (int, int) {
	var fuel int
	var extra int

	for _, mass := range *masses {
		calc := (mass / 3) - 2
		fuel += calc
		extra += Task2(calc)
	}

	return fuel, extra
}

func Task2(mass int) int {
	var fuel int
	var sum int

	fuel = (mass / 3) - 2

	if fuel > 0 {
		sum += fuel + Task2(fuel)
	} else {
		return 0
	}

	return sum
}
