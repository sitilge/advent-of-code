package day3

import (
	"bytes"
	"git.sitilge.id.lv/advent-of-code/src/helpers"
	"log"
	"os"
	"testing"
)

var inputs1 [][]byte
var inputs2 [][]byte

func TestMain(m *testing.M) {
	file, err := os.Open("./../../inputs/day3.txt")
	if err != nil {
		log.Fatal("unable to open inputs file")
	}
	defer file.Close()

	lines := helpers.ReadLines(file)

	inputs1 = bytes.Split(lines[0], []byte{','})
	inputs2 = bytes.Split(lines[1], []byte{','})

	code := m.Run()

	os.Exit(code)
}

func TestTasks(t *testing.T) {
	//TODO - test succeeds yet the complexity is n^2, a brute force algo
	t.Skip()

	distance, signal := Tasks(inputs1, inputs2)

	if distance != 1285 {
		t.Fail()
	}

	if signal != 14228 {
		t.Fail()
	}
}
