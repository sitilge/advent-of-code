package day3

import (
	"fmt"
	"math"
	"strconv"
)

type Point struct {
	x int
	y int
}

func Tasks(line1 [][]byte, line2 [][]byte) (int, int) {
	coords1 := CalcCoords(line1)
	coords2 := CalcCoords(line2)

	var distance int
	var signal int

	for i, coord1 := range coords1 {
		for j, coord2 := range coords2 {
			if coord1.x == coord2.x && coord1.y == coord2.y {
				x := int(math.Abs(float64(coord1.x)))
				y := int(math.Abs(float64(coord1.y)))

				if x+y < distance || distance == 0 {
					distance = x + y
				}

				if i+j < signal || signal == 0 {
					// + 2 to include the first steps for each cable
					signal = i + j + 2
				}
			}
		}
	}

	return distance, signal
}

func CalcCoords(line [][]byte) []Point {
	var coords []Point
	for _, path := range line {
		value, err := strconv.Atoi(string(path[1:]))

		if err != nil {
			fmt.Printf("Unable to convert to integer %v \n", string(path[1:]))

			continue
		}

		var x, y int

		for i := 0; i < value; i++ {
			if len(coords) > 0 {
				last := coords[len(coords)-1]

				x = last.x
				y = last.y
			}

			point := Point{
				x: x,
				y: y,
			}

			switch path[0] {
			case 'D':
				point.y--
			case 'U':
				point.y++
			case 'L':
				point.x++
			case 'R':
				point.x--
			}

			coords = append(coords, point)
		}
	}

	return coords
}
