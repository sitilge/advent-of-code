package day5

import (
	"bytes"
	"git.sitilge.id.lv/advent-of-code/src/helpers"
	"log"
	"os"
	"testing"
)

var inputs []int

func TestMain(m *testing.M) {
	file, err := os.Open("./../../inputs/day5.txt")
	if err != nil {
		log.Fatal("unable to open inputs file")
	}
	defer file.Close()

	lines := helpers.ReadLines(file)

	split := bytes.Split(lines[0], []byte{','})

	inputs = helpers.ParseInt(split)

	code := m.Run()

	os.Exit(code)
}

func TestTask1(t *testing.T) {
	if Task1(inputs, 1) != 9025675 {
		t.Fail()
	}
}

func TestTask2(t *testing.T) {
	//if Task2(inputs, 5) != 9025675 {
	//	t.Fail()
	//}
}
