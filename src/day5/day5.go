package day5

import (
	"fmt"
	"log"
	"strconv"
)

func Task1(opcodes []int, input int) int {
	if len(opcodes) < 4 {
		log.Fatal("Invalid opcode length")
	}

	var diagnostic int
	blob := make([]int, len(opcodes))
	copy(blob, opcodes)

	for i := 0; i < len(blob); i += 1 {
		converted := []byte(strconv.Itoa(blob[i]))
		
		var opcode int
		modes := make([]int, 0)
		var v1, v2 int
		var err error

		if len(converted) > 1 {
			opcode, err = strconv.Atoi(string(converted[len(converted) - 2:]))

			if err != nil {
				log.Fatal(err)
			}
			
			for j := len(converted) - 3; j >= 0; j-- {
				mode, err := strconv.Atoi(string(converted[j]))

				if err != nil {
					log.Fatal(err)
				}

				modes = append(modes, mode)
			}
		} else {
			opcode = blob[i]
		}
		
		if opcode == 99 {
			diagnostic = blob[i + 1]
			break
		}

		if len(modes) == 0 {
			v1 = blob[blob[i+1]]

			if opcode == 1 || opcode == 2 {
				v2 = blob[blob[i+2]]
			}
		} else if len(modes) == 1 {
			if modes[0] == 0 {
				v1 = blob[blob[i+1]]
			} else {
				v1 = blob[i+1]
			}

			if opcode == 1 || opcode == 2 {
				v2 = blob[blob[i+2]]
			}
		} else {
			if modes[0] == 0 {
				v1 = blob[blob[i+1]]
			} else {
				v1 = blob[i+1]
			}

			if opcode == 1 || opcode == 2 {
				if modes[1] == 0 {
					v2 = blob[blob[i+2]]
				} else {
					v2 = blob[i+2]
				}
			}
		}

		switch opcode {
		case 1:
			blob[blob[i+3]] = v1 + v2
			
			i += 3
		case 2:
			blob[blob[i+3]] = v1 * v2

			i += 3
		case 3:
			blob[blob[i+1]] = input

			i += 1
		case 4:
			fmt.Printf("%v\n", v1)

			i += 1
		case 5:
			if modes[0] != 0 {
				i = blob[modes[1] - 1]
			}
		case 6:
			if modes[0] == 0 {
				i = blob[modes[1] - 1]
			}
		case 7:
			if modes[0] < modes[1] {
				blob[modes[2]] = 1
			} else {
				blob[modes[2]] = 0
			}
		case 8:
			if modes[0] == modes[1] {
				blob[modes[2]] = 1
			} else {
				blob[modes[2]] = 0
			}
		}
	}

	return diagnostic
}
